import {NumberGeneratorService} from "../services/NumberGeneratorService";
import {CartesianCoordinate} from "./CartesianCoordinate";
import {AxisRotationAngles} from "./AxisRotationAngles";

export class OrbitalObject {
    readonly _R: number = 6371000;                 // Earth radius in m
    readonly _M: number = 5.97e24;                 // Earth mass in kg
    readonly _G: number = 6.67408e-11;             // Earth Gravity constant
    readonly _PI: number = 3.14159265359;          // Pi constant

    readonly _randomNumberGenerator: NumberGeneratorService = new NumberGeneratorService();
    readonly _orbitalHeight: number;
    readonly _angular_velocity: number;
    readonly _axisRotationAngles: AxisRotationAngles;

    private _localPosition: CartesianCoordinate;
    absolutePosition: CartesianCoordinate;

    constructor(orbitalHeight: number) {
        let angleX = this._randomNumberGenerator.generateRandomDouble(0, this._PI * 2);
        let angleY = this._randomNumberGenerator.generateRandomDouble(0, this._PI * 2);
        let angleZ = this._randomNumberGenerator.generateRandomDouble(0, this._PI * 2);
        this._axisRotationAngles = new AxisRotationAngles(angleX, angleY, angleZ);

        this._orbitalHeight = orbitalHeight * 1000;
        this._localPosition = new CartesianCoordinate(this._orbitalHeight + this._R, 0, 0);
        this.absolutePosition = this.getAbsolutePosition();
        let velocity = Math.sqrt(this._G * this._M / (this._R + this._orbitalHeight));
        this._angular_velocity = velocity / (this._R + this._orbitalHeight);
    }

    calculatePositionAfterTime(timeInSec: number) {
        this._localPosition = this.rotateAroundZ(this._localPosition, this._angular_velocity * timeInSec);
        this.absolutePosition = this.getAbsolutePosition();
    }

    private getAbsolutePosition(): CartesianCoordinate {
        let point = this.rotateAroundX(this._localPosition, this._axisRotationAngles.angleX);
        point = this.rotateAroundY(point, this._axisRotationAngles.angleY);
        point = this.rotateAroundZ(point, this._axisRotationAngles.angleZ);
        return point;
    }

    private rotateAroundX(point: CartesianCoordinate, angle: number): CartesianCoordinate {
        let x = point.x;
        let y = point.y * Math.cos(angle) - point.z * Math.sin(angle);
        let z = point.y * Math.sin(angle) + point.z * Math.cos(angle);
        return new CartesianCoordinate(x, y, z);
    }

    private rotateAroundY(point: CartesianCoordinate, angle: number): CartesianCoordinate {
        let x = point.x * Math.cos(angle) + point.z * Math.sin(angle);
        let y = point.y;
        let z = -point.x * Math.sin(angle) + point.z * Math.cos(angle);
        return new CartesianCoordinate(x, y, z);
    }

    private rotateAroundZ(point: CartesianCoordinate, angle: number): CartesianCoordinate {
        let x = point.x * Math.cos(angle) - point.y * Math.sin(angle);
        let y = point.x * Math.sin(angle) + point.y * Math.cos(angle);
        let z = point.z;
        return new CartesianCoordinate(x, y, z);
    }

}

import * as THREE from "three";
import {SphereThreeJs} from "./SphereThreeJs";

export class EarthThreeJs extends SphereThreeJs {
    constructor(scene: THREE.Scene, color: number) {
        super(scene, color, 6371, {x: 0, y: 0, z: 0 });
    }
}
import * as THREE from "three";
import {OrbitalObject} from "../OrbitalObject";
import {SphereThreeJs} from "./SphereThreeJs";

export class OrbitalObjectThreeJs extends SphereThreeJs {
    readonly _timePerIterationInSec: number = 5;
    private _trajectoryLine: THREE.Line | null = null;
    private _trajectoryPoints: THREE.Vector3[] = [];
    private _orbitalObject: OrbitalObject;

    constructor(scene: THREE.Scene, color: number, radius: number, orbitalHeight: number) {
        super(scene, color, radius, {x: 6371 + orbitalHeight, y: 0, z: 0});
        this._orbitalObject = new OrbitalObject(orbitalHeight);
        this._trajectoryPoints.push(new THREE.Vector3(
            this._orbitalObject.absolutePosition.x / 1000,
            this._orbitalObject.absolutePosition.y / 1000,
            this._orbitalObject.absolutePosition.z / 1000));
    }

    makeStepOnOrbit() {
        this._orbitalObject.calculatePositionAfterTime(this._timePerIterationInSec);
        this.setPosition(
            this._orbitalObject.absolutePosition.x / 1000,
            this._orbitalObject.absolutePosition.y / 1000,
            this._orbitalObject.absolutePosition.z / 1000);
        this._trajectoryPoints.push(new THREE.Vector3(
            this._orbitalObject.absolutePosition.x / 1000,
            this._orbitalObject.absolutePosition.y / 1000,
            this._orbitalObject.absolutePosition.z / 1000));
        this.drawTrajectory();
    }

    private drawTrajectory() {
        if (this._trajectoryLine !== null)
            this._scene.remove(this._trajectoryLine);
        const geometry = new THREE.BufferGeometry().setFromPoints(this._trajectoryPoints);
        const material = new THREE.LineBasicMaterial( {color: this._color} );
        this._trajectoryLine = new THREE.Line(geometry,material);
        this._scene.add(this._trajectoryLine);
    }
}
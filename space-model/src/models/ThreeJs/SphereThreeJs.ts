import * as THREE from "three";
import {CartesianCoordinate} from "../CartesianCoordinate";

export class SphereThreeJs {
    protected _scene: THREE.Scene;
    protected _body: THREE.Mesh;
    protected _edges: THREE.LineSegments;
    protected _color: number;

    constructor(scene: THREE.Scene, color: number, radius: number, position: CartesianCoordinate) {
        this._scene = scene;
        this._color = color;

        const geometryBody = new THREE.SphereGeometry(radius, 16, 16);
        const materialBody = new THREE.MeshBasicMaterial({color: color});
        this._body = new THREE.Mesh(geometryBody,materialBody);
        this._body.position.set(position.x, position.y, position.z);
        scene.add(this._body);

        const geometryEdges = new THREE.EdgesGeometry(geometryBody);
        const materialEdges = new THREE.LineBasicMaterial({color: color + 100});
        this._edges = new THREE.LineSegments(geometryEdges, materialEdges);
        this._edges.position.set(position.x, position.y, position.z);
        scene.add(this._edges);
    }

    protected setPosition(x: number, y: number, z: number) {
        this._body.position.set(x, y, z);
        this._edges.position.set(x, y, z);
    }
}
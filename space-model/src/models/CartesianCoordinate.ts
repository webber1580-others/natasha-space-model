export class CartesianCoordinate {
    constructor (public x: number,
                 public y: number,
                 public z: number) {}
}

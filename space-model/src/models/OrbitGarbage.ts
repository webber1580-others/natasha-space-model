export class OrbitGarbage {
    readonly _leftBoundOrbitHeight: number;
    readonly _rightBoundOrbitHeight: number;
    readonly _garbageCount: number;

    constructor(leftBoundOrbitHeight: number, rightBoundOrbitHeight: number, garbageCount: number) {
        this._leftBoundOrbitHeight = leftBoundOrbitHeight;
        this._rightBoundOrbitHeight = rightBoundOrbitHeight;
        this._garbageCount = garbageCount;
    }
}
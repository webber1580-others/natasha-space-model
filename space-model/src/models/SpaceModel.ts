import {OrbitGarbage} from "./OrbitGarbage";
import {OrbitalObject} from "./OrbitalObject";

export class SpaceModel {
    readonly _orbitalGarbageDistribution: OrbitGarbage[] = [
        new OrbitGarbage(400, 500, 4731),
        new OrbitGarbage(500, 600, 9103),
        new OrbitGarbage(600, 700, 14951),
        new OrbitGarbage(700, 800, 32262),
        new OrbitGarbage(800, 900, 47496),
        new OrbitGarbage(900, 1000, 48117),
        new OrbitGarbage(1000, 1100, 22249),
        new OrbitGarbage(1100, 1200, 5929),
        new OrbitGarbage(1200, 1300, 9133),
        new OrbitGarbage(1300, 1400, 25393),
        new OrbitGarbage(1400, 1500, 41979),
        new OrbitGarbage(1500, 1600, 3564),
        new OrbitGarbage(1600, 1700, 1863),
        new OrbitGarbage(1700, 1800, 1490),
        new OrbitGarbage(1800, 1900, 1489),
        new OrbitGarbage(1900, 2000, 1447),
    ];
    readonly _timePerIterationInSec: number = 5;
    private _spaceShip: OrbitalObject;
    private _spaceShipSize: number;
    private _orbitalGarbageList: OrbitalObject[];

    getCollapseProbability(orbitalRadius: number, spaceShipSize: number, garbageCount: number, daysCount: number): number {
        this.initializeData(orbitalRadius, spaceShipSize, garbageCount);
        let collapseNumber = 0;
        for (let i = 0; i < daysCount * 24 * 60 * 60; i++) {
            for (let garbage of this._orbitalGarbageList) {
                if (this.isCollisionOccurred(garbage)) {
                    collapseNumber++;
                }
                garbage.calculatePositionAfterTime(this._timePerIterationInSec);
            }
            this._spaceShip.calculatePositionAfterTime(this._timePerIterationInSec);
            if (i % (24 * 60 * 60) == 0) {
                console.log(`Day ${i % (24 * 60 * 60)}`)
            }
        }
        return collapseNumber;
    }

    getOrbitalGarbageCount(orbitalRadius: number): number {
        let result = 0;
        for (let orbitGarbage of this._orbitalGarbageDistribution) {
            if (orbitGarbage._leftBoundOrbitHeight <= orbitalRadius && orbitalRadius <= orbitGarbage._rightBoundOrbitHeight) {
                result =  orbitGarbage._garbageCount;
            }
        }
        return result;
    }

    private isCollisionOccurred(garbage: OrbitalObject): boolean {
        let spaceShipCoordinates = this._spaceShip.absolutePosition;
        let garbageCoordinates = garbage.absolutePosition;
        let distance = Math.sqrt(
            Math.pow(spaceShipCoordinates.x - garbageCoordinates.x,2) +
            Math.pow(spaceShipCoordinates.y - garbageCoordinates.y,2) +
            Math.pow(spaceShipCoordinates.z - garbageCoordinates.z,2));

        return distance <= this._spaceShipSize;
    }

    private initializeData(orbitalRadius: number, spaceShipSize: number, garbageCount: number) {
        this._spaceShip = new OrbitalObject(orbitalRadius);
        this._spaceShipSize = spaceShipSize;
        this._orbitalGarbageList = [];
        for (let i = 0; i < garbageCount; i++)
            this._orbitalGarbageList.push(new OrbitalObject(orbitalRadius));
    }
}
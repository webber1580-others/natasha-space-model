export class AxisRotationAngles {
    constructor (public angleX: number,
                 public angleY: number,
                 public angleZ: number) {}
}

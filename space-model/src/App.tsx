import React from 'react';
import ThreeJsComponent from "./components/ThreeJsComponent";
import {FormComponent} from "./components/FormComponent";

function App() {
  return (
    <div className="App">
        <FormComponent />
    </div>
  );
}

export default App;

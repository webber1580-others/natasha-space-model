export class NumberGeneratorService {
    generateRandomInt(leftValue: number, rightValue: number) {
        return Math.floor(Math.random() * (rightValue - leftValue) + leftValue);
    }

    generateRandomDouble(leftValue: number, rightValue: number) {
        return Math.random() * (rightValue - leftValue) + leftValue;
    }
}

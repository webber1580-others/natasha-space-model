import React from "react";
import {SpaceModel} from "../models/SpaceModel";

type IProps = {

}

type IState = {
    orbitalRadius: number,
    spaceShipSize: number,
    garbageCount: number,
    daysCount: number,
    collapseProbability: number,
}

export class FormComponent extends React.Component<IProps, IState> {
    readonly _spaceModel: SpaceModel = new SpaceModel();

    constructor(props: IProps) {
        super(props);
        this.state = {
            orbitalRadius: 1000,
            spaceShipSize: 100,
            garbageCount: 48117,
            daysCount: 365,
            collapseProbability: 0,
        }
    }

    private onRadiusChange(event: any) {
        let orbitalObject = event.target.value;
        let garbageCount = this._spaceModel.getOrbitalGarbageCount(event.target.value);
        this.setState({
            orbitalRadius: orbitalObject,
            garbageCount: garbageCount
        });
    }

    private onSpaceShipSizeChange(event: any) {
        this.setState({
            spaceShipSize: event.target.value
        });
    }

    private onGarbageCountChange(event: any) {
        this.setState({
            garbageCount: event.target.value
        });
    }

    private onDaysCountChange(event: any) {
        this.setState({
            daysCount: event.target.value
        });
    }

    private calculateCollapseProbability() {
        this.setState({
            collapseProbability: this._spaceModel.getCollapseProbability(
                this.state.orbitalRadius,
                this.state.spaceShipSize,
                this.state.garbageCount,
                this.state.daysCount)
        });
    }

    render () {
        return (
            <>
                <div>
                    <label>Введите радиус орбиты (km): </label>
                    <input type="text" name="radius" value={this.state.orbitalRadius} onChange={event => this.onRadiusChange(event)}/>
                </div>
                <div>
                    <label>Размер космического коробля (m): </label>
                    <input type="text" name="spaceShipSize" value={this.state.spaceShipSize} onChange={event => this.onSpaceShipSizeChange(event)}/>
                </div>
                <div>
                    <label>Число частиц на орбите: </label>
                    <input type="text" name="garbageCount" value={this.state.garbageCount} onChange={event => this.onGarbageCountChange(event)}/>
                </div>
                <div>
                    <label>Время моделирования (days): </label>
                    <input type="text" name="daysCount" value={this.state.daysCount} onChange={event => this.onDaysCountChange(event)}/>
                </div>
                <button onClick={() => this.calculateCollapseProbability()}>Рассчитать</button>
                <div>
                    <label>Вероятность столкновения: </label>
                    <input type="text"
                           name="collapseProbability"
                           value={this.state.collapseProbability}
                    />
                </div>
            </>
        );
    }
}

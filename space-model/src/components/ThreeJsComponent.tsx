import * as THREE from "three";
import React from 'react';
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import {EarthThreeJs} from "../models/ThreeJs/EarthThreeJs";
import {OrbitalObjectThreeJs} from "../models/ThreeJs/OrbitalObjectThreeJs";

export class ThreeJsComponent extends React.Component<any, any> {
    componentDidMount() {
        // === SCENE CONFIGURATION ===
        let scene = new THREE.Scene();
        let camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 50000 );
        let renderer = new THREE.WebGLRenderer();
        renderer.setSize( window.innerWidth, window.innerHeight );
        document.body.appendChild( renderer.domElement );

        let controls = new OrbitControls( camera, renderer.domElement );
        camera.position.set( 0, 0, 20000 );
        controls.update();

        let axesHelper = new THREE.AxesHelper( 20000 );
        scene.add(axesHelper);

        // ==========================

        // === SCENE DRAWING ===

        let earth = new EarthThreeJs(scene, 0x2194ce);
        let orbitalObject = new OrbitalObjectThreeJs(scene, 0xff5370, 100, 1000);
        let orbitalObjects: OrbitalObjectThreeJs[] = [];
        for(let i = 0; i < 10; i++)
            orbitalObjects.push(new OrbitalObjectThreeJs(scene, 0xfeff31, 50, 1000));

        // ======================

        var animate = () => {
            requestAnimationFrame(animate);
            controls.update();

            // === ANIMATION ACTIONS ===
            orbitalObject.makeStepOnOrbit();
            for(let i = 0; i < 10; i++)
                orbitalObjects[i].makeStepOnOrbit();

            // =========================
            renderer.render(scene, camera);
        };
        animate();
    }

    render() {
        return (
            <div />
        );
    }
}

export default ThreeJsComponent;
